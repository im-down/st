# st - simple terminal
this is my build of the suckless st. its not that complicated.

only applied one patch (scrollback)
also changed font to Source Code Variable (needed for install unless you change it)

install:

```
sudo make clean install 
```

uninstall:

```
sudo make uninstall
```
